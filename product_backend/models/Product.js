const mongoose = require('mongoose')
const { Schema } = mongoose
const producSchema = Schema({
  name: String,
  Price: Number
})

module.exports = mongoose.model('Product', producSchema)
