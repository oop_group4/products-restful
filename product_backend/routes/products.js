const express = require('express')
const router = express.Router()
const Product = require('../models/Product')
let lastId = 11
const getProducts = async function (req, res, next) {
  try{
    const product = await Product.find({}).exec()
    res.json(product)
  } catch(err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const getProduct = async function (req, res, nextd) {
  const id = req.params.id
  console.log(id)
  try {
    const product = await Product.findById(id).exec()
    if(product===null) {
      return res.status(404).json({
        message: 'Product not found!!'
  
    })
    }
    res.json(product)
  } catch (err) {
    return res.status(404).json({
      message: err.message

  })
  }
}

const addProducts = async function (req, res, next) {
 // console.log(req.body)
  //const newProduct = {
   // id: lastId,
   // name: req.body.name,
   // price: parseFloat(req.body.price)
 // }
 // products.push(newProduct)
  //lastId++
  const newProduct = new Product({
    name: req.body.name,
    price: parseFloat(req.body.price)
  })
  try {
    await newProduct.save()
    res.status(201).json(newProduct)
  } catch(err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const updateProducts = async function (req, res, next) {
  const productID = req.params.id
  try{
    const product = await Product.findById(productID)
    product.name = req.body.name
    product.price = parseFloat(req.body.price)
    await product.save()
    res.status(200).json(product)
  } catch (err) {
    return res.status(404).send({ message: err.message })
}}
}
    const deleteProducts = async function (req, res, next) {
        const productID = res.params.id
       try {
        await Product.findByIdAndDelete(productID)
        return res.status(200).send()
       } catch (err) {
         return res.status(404).send({ message: err.message})
       }
   // const index = product.findIndex(function(item){
   //     return item.id == productID
   // })
   //     if (index >= 0) {
   //         products.splice(index, 1)
   //         res.status(200).send()
   //     }else{
   //         res.status(404).json({
   //             code: 404,
    //            msg: 'No product id' + res.params.id
        
   //         })
    //    }
    }
router.get('/', getProducts)
router.post('/', addProducts)
router.get('/:id', getProduct)
router.get('/:id', updateProduct)
router.get('/:id', deleteProduct)

module.exports = router